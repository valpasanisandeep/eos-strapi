'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/v3.x/concepts/configurations.html#bootstrap
 */

module.exports = async () => {
  const count = await strapi.query('applications').count()

  /* Skip popuating when it's not with Docker */
  if(!process.env.WITH_DOCKER) return

  if(count >= 1) return

  await strapi.services.applications.create({
    brand: "Demo application",
    showChangelog: true
  });
};
